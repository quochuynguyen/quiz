from quiz.models import Questions, Images, Level1, Level2, Level3
from rest_framework import serializers


class ImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Images
        fields = ('id', 'question', 'image')


class Level1Serializer(serializers.ModelSerializer):

    class Meta:
        model = Level1
        fields = ('id', 'code', 'name')


class Level2Serializer(serializers.ModelSerializer):

    class Meta:
        model = Level2
        fields = ('id', 'code', 'name', 'level1')


class Level3Serializer(serializers.ModelSerializer):

    class Meta:
        model = Level3
        fields = ('id', 'code', 'name', 'level2')



class LevelsSerializer(serializers.Serializer):
    level1s = Level1Serializer(many=True)
    level2s = Level2Serializer(many=True)
    level3s = Level3Serializer(many=True)


class QuestionsSerializer(serializers.ModelSerializer):
    images = ImagesSerializer(many=True, read_only=True)
    # explanation = serializers.CharField(max_length=2000, allow_null=True)

    class Meta:
        model = Questions
        fields = ('id', 'text', 'choice1', 'choice2', 'choice3', 'choice4', 'explanation', 'images', 'level1', 'level2', 'level3')

    def add_images(self, instance):
        """ Save list of image objects. Image files have already been uploaded, just need to associate them with question """
        for img in self.initial_data['images']:
            if 'id' in img:
                image = Images.objects.filter(pk=img['id']).first()
                if image:
                    instance.images.add(image)

    def create(self, validated_data):
        instance = super().create(validated_data)
        self.add_images(instance)
        return instance

    def update(self, instance, validated_data):
        self.add_images(instance)
        return super().update(instance, validated_data)
