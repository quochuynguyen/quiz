from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class CustomPagination(PageNumberPagination):
    page_size = 5
    page_size_query_param = "page_size"
    max_page_size = 1000

    def get_paginated_response(self, data):

        page_links = self.get_html_context()['page_links']

        return Response({
            'count': self.page.paginator.count,
            'next': self.get_next_link(),
            'previous': self.get_previous_link(),
            'page_links': [pl._asdict() for pl in page_links],
            'results': data
        })
