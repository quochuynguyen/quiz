from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
from rest_framework import viewsets, filters
from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from quiz.models import Questions, Level1, Level2, Level3, Images
from quiz.serializers import QuestionsSerializer, ImagesSerializer, Level1Serializer, Level2Serializer, Level3Serializer, LevelsSerializer
from quiz.pagination import CustomPagination


class Levels(object):
    def __init__(self, level1s, level2s, level3s):
        self.level1s = level1s
        self.level2s = level2s
        self.level3s = level3s


class ListLevels(APIView):
    def get(self, request, format=None):
        levels = Levels(level1s=Level1.objects.all(), level2s=Level2.objects.all(), level3s=Level3.objects.all())
        serializer = LevelsSerializer(levels)
        return JsonResponse(serializer.data)


class ImagesViewSet(viewsets.ModelViewSet):
    queryset = Images.objects.all()
    serializer_class = ImagesSerializer


class QuestionsViewSet(viewsets.ModelViewSet):
    queryset = Questions.objects.all().order_by('-modified')
    serializer_class = QuestionsSerializer
    pagination_class = CustomPagination
    filter_backends = (filters.SearchFilter, DjangoFilterBackend)
    search_fields = ('text', '^level1__name', '^level2__name', '^level3__name')
    filter_fields = ('level1__code', 'level2__code', 'level3__code')


class Level1ViewSet(viewsets.ModelViewSet):
    queryset = Level1.objects.all()
    serializer_class = Level1Serializer


class Level2ViewSet(viewsets.ModelViewSet):
    queryset = Level2.objects.all()
    serializer_class = Level2Serializer


class Level3ViewSet(viewsets.ModelViewSet):
    queryset = Level3.objects.all()
    serializer_class = Level3Serializer


@csrf_exempt
def upload(request):
    if request.method == 'POST' and request.FILES['file']:
        myfile = request.FILES['file']
        image = Images(image=myfile)
        image.save()
        return JsonResponse({'id': image.id, 'image': image.image.url})
