from django.db import models


class Questions(models.Model):
    text = models.CharField(max_length=1000)
    choice1 = models.CharField(max_length=50)  # Correct answer
    choice2 = models.CharField(max_length=50)
    choice3 = models.CharField(max_length=50)
    choice4 = models.CharField(max_length=50)
    explanation = models.CharField(max_length=2000, blank=True, null=True)
    level1 = models.ForeignKey('Level1', on_delete=models.CASCADE)
    level2 = models.ForeignKey('Level2', on_delete=models.CASCADE, null=True)
    level3 = models.ForeignKey('Level3', on_delete=models.CASCADE, null=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "%s %s" % (self.text, self.choice1)


class Images(models.Model):
    image = models.FileField()
    question = models.ForeignKey(Questions, on_delete=models.CASCADE, null=True, related_name='images')


class Level1(models.Model):
    code = models.CharField(max_length=10, unique=True)
    name = models.CharField(max_length=50, unique=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    def __str__(self):
        return "Level1 %s %s" % (self.code, self.name)


class Level2(models.Model):
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    level1 = models.ForeignKey(Level1, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('code', 'level1')

    def __str__(self):
        return "Level2 %s %s" % (self.code, self.name)


class Level3(models.Model):
    code = models.CharField(max_length=10)
    name = models.CharField(max_length=50)
    level2 = models.ForeignKey(Level2, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('code', 'level2')

    def __str__(self):
        return "Level3 %s %s" % (self.code, self.name)
