// Vue.use(VueLocalStorage);


Vue.use(VueTransmit);


Vue.component('question-item', {
    props: ['question', 'levels'],
    template: '#question-item-template',
    computed: {
        // Get level obj based on level id
        level1: function() {
            var self = this;
            return this.levels.level1s.filter(function(level1) {
                return level1.id === self.question.level1;
            })[0];
        },
        level2: function() {
            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.id === self.question.level2;
            })[0];
        },
        level3: function() {
            var self = this;
            return this.levels.level3s.filter(function(level3) {
                return level3.id === self.question.level3;
            })[0];
        }
    },
    methods: {
        deleteQuestion: function(question) {
            this.$emit("deletequestion", question);
        }
    }
});

Vue.component('question-edit', {
    props: ['question', 'levels'],
    template: '#question-edit-template',
    data: function() {
        return {
            vue_transmit_options: {
                acceptedFileTypes: ['image/*']
            }
        };
    },
    computed: {
        // Get all sub-levels based on a parent level (level1 > level2 > level3)
        possibleLevel2s: function() {
            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.level1 === self.question.level1;
            });
        },
        possibleLevel3s: function() {
            var self = this;
            return this.levels.level3s.filter(function(level3) {
                return level3.level2 === self.question.level2;
            });
        }
    },
    methods: {
        uploadSuccess: function(file, response) {
            this.question.images.push(response);
        },
        delete_image: function(image) {
            var self = this;
            return axios.delete("/images/" + image.id + "/")
                .then(function(response) {
                    var deleteIndex = self.question.images.indexOf(image);
                    self.question.images.splice(deleteIndex, 1);
                    return true;
                })
                .catch(function(error) {
                    console.error(error.response.data);
                });
        }
    }
});

Vue.component('add-level', {
    props: ['levels'],
    template: "#add-level-template",
    data: function() {
        return {
            enteredLevel: {code: null, name: null, level1: null, level2: null, level3: null},
            status: {message: null, error: false}
        };
    },
    computed: {
        possibleLevel2s: function() {

            var self = this;
            return this.levels.level2s.filter(function(level2) {
                return level2.level1 === self.enteredLevel.level1;
            });
        }
    },
    methods: {
        addLevelx: function(lvltype) {
            var self = this;
            return axios.post('/' + lvltype + '/', self.enteredLevel)
                .then(function(response) {
                    // self.enteredLevel = response.data;
                    self.status = {message: "Code: " + self.enteredLevel.code + " Name: " + self.enteredLevel.name + " added.", error: false};
                    self.$emit('addlevel', lvltype, response.data);
                })
                .catch(function(error) {
                    self.status = {message: error.response.data, error: true};
                });
        }
    }
});

var app = new Vue({
    el: '#app',
    data: {
        QUESTIONS_PER_PAGE: 50,
        questions: [],
        levels: {
            level1s: [
                {id: 1, name:'Math', code: 'M'},
            ],
            level2s: [
                {id: 11, level1: 1, name:'Chapter 1', code: 'C1'},
            ],
            level3s: [
                {id: 31, level2: 11, name:'Easy', code: 'E'},
            ]
        },

        editingQuestion: {id: null, level1: null, level2: null, level3: null, text: '', choice1: '', choice2: '', choice3: '', choice4: '', images: [], explanation: ''}, //question that being edit
        isEditing: false, //2 mutually exclusive UI modes: Edit or Add
        selectedQuestion: null, //current selected question on the list

        page_links: [],
        searchQuery: null,
        statusMessage: {message: null, error: false}
    },
    mounted: function() {
        //Get data from persistence storage
        this.get_levels();
        this.get_questions();

    },
    methods: {
        selectToEdit: function(question, event) {
            this.isEditing = true;
            this.selectedQuestion = question;
            this.editingQuestion = question;
            this.statusMessage = {message: null, error: false};
        },
        save: function(event) {
            var vm = this;
            if (this.editingQuestion.text !== '') {
                if (!this.isEditing) {  // New

                    this.post_question(this.editingQuestion).then(function(saved) {
                        if (saved) {
                            vm.questions.push(vm.editingQuestion);
                            vm.add();
                        }
                    });
                    
                }
                else
                    this.put_question(this.editingQuestion);

                
            }
        },
        add: function(event) {
            this.editingQuestion = {id: null, level1: null, level2: null, level3: null, text: '', choice1: '', choice2: '', choice3: '', choice4: '', images: [], explanation: ''};
            this.selectedQuestion = this.editingQuestion;
            this.isEditing = false;
            this.statusMessage = {message: null, error: false};
        },

        get_levels: function() {
            var vm = this;
            return axios.get('/levels/')
                .then(function(response) {
                    vm.levels = response.data;
                })
                .catch(function(error) {
                    vm.statusMessage.message = error.response.data;
                    vm.statusMessage.error = true;
                });
        },
        get_questions: function(apiUrl) {
            var vm = this;

            apiUrl = apiUrl || '/questions/?page_size=' + this.QUESTIONS_PER_PAGE;
            if (this.searchQuery && apiUrl.indexOf('search') < 0)
                apiUrl = apiUrl + "&search=" + this.searchQuery;

            return axios.get(apiUrl)
                .then(function(response) {
                    vm.questions = response.data.results;
                    vm.page_links = response.data.page_links;
                    vm.add();
                })
                .catch(function(error) {
                    vm.statusMessage.message = error.response.data;
                    vm.statusMessage.error = true;
                });
        },
        post_question: function(question) {
            var vm = this;
            return axios.post('/questions/', question)
                .then(function(response) {

                    vm.editingQuestion = response.data;
                    vm.statusMessage.message = "Saved";
                    vm.statusMessage.error = false;
                    return true;
                })
                .catch(function(error) {
                    vm.statusMessage.message = error.response.data;
                    vm.statusMessage.error = true;
                });
        },
        put_question: function(question) {
            var vm = this;
            return axios.put("/questions/" + question.id + "/", question)
                .then(function(response) {
                    vm.question = response.data;
                    vm.statusMessage = {message: "Saved", error: false};
                    return true;
                })
                .catch(function(error) {
                    vm.statusMessage.message = error.response.data;
                    vm.statusMessage.error = true;

                });
        },
        delete_question: function(question) {
            var vm = this;
            return axios.delete("/questions/" + question.id + "/")
                .then(function(response) {
                    var deleteIndex = vm.questions.indexOf(question);
                    vm.questions.splice(deleteIndex, 1);
                    vm.add();
                    return true;
                })
                .catch(function(error) {
                    vm.statusMessage.message = error.response.data;
                    vm.statusMessage.error = true;
                });
        },
        add_level: function(lvltype, level) {
            var clone = JSON.parse(JSON.stringify(level));
            if (lvltype === "level1")
                this.levels.level1s.push(clone);
            else if (lvltype === "level2")
                this.levels.level2s.push(clone);
            else if (lvltype === "level3")
                this.levels.level3s.push(clone);
        }
        
    },
    filters: {
        json: function(value) {
            return JSON.stringify(value, null, 2);
        }
    }
});

